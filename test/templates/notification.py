# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

"""xdg desktop portals mock template"""


from test.templates import Request, ASVType
from test import response
from typing import Dict, List, Tuple, Iterator


import dbus.service

BUS_NAME = "org.freedesktop.portal.Desktop"
MAIN_OBJ = "/org/freedesktop/portal/desktop"
SYSTEM_BUS = False
MAIN_IFACE = "org.freedesktop.portal.Notification"


def load(mock, parameters=None):
    mock.AddProperties(
        MAIN_IFACE,
        dbus.Dictionary({"version": dbus.UInt32(parameters.get("version", 1))}),
    )

    mock.AddMethod(MAIN_IFACE, "AddNotification", "sa{sv}", "", "")
    mock.AddMethod(MAIN_IFACE, "RemoveNotification", "s", "", "")
