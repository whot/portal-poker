# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

"""xdg desktop portals mock template"""


from test.templates import ASVType
from typing import Dict, List, Tuple, Iterator


import dbus.service

BUS_NAME = "org.freedesktop.portal.Desktop"
MAIN_OBJ = "/org/freedesktop/portal/desktop"
SYSTEM_BUS = False
MAIN_IFACE = "org.freedesktop.portal.NetworkMonitor"


def load(mock, parameters=None):
    # return values for each method, as a list, used up in-order
    return_values: Dict[str, List[Tuple[int, ASVType]]] = parameters.get(
        "return_values", {}
    )
    mock.return_values: Dict[str, Iterator] = {
        m: iter(r) for m, r in return_values.items()
    }

    mock.AddProperties(
        MAIN_IFACE,
        dbus.Dictionary({"version": dbus.UInt32(parameters.get("version", 3))}),
    )


@dbus.service.method(
    MAIN_IFACE,
    in_signature="",
    out_signature="b",
)
def GetAvailable(self):
    try:
        res = next(self.return_values["GetAvailable"])
    except (KeyError, StopIteration):
        res = {"available": True}

    return res["available"]


@dbus.service.method(
    MAIN_IFACE,
    in_signature="",
    out_signature="b",
)
def GetMetered(self):
    try:
        res = next(self.return_values["GetMetered"])
    except (KeyError, StopIteration):
        res = {"metered": True}

    return res["metered"]


@dbus.service.method(
    MAIN_IFACE,
    in_signature="",
    out_signature="u",
)
def GetConnectivity(self):
    try:
        res = next(self.return_values["GetConnectivity"])
    except (KeyError, StopIteration):
        res = {"connectivity": 4}

    return res["connectivity"]


@dbus.service.method(
    MAIN_IFACE,
    in_signature="",
    out_signature="a{sv}",
)
def GetStatus(self):
    try:
        res = next(self.return_values["GetStatus"])
    except (KeyError, StopIteration):
        res = {"available": True, "metered": False, "connectivity": 4}

    return res
