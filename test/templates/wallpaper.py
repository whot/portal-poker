# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

"""xdg desktop portals mock template"""

from test.templates import Request, ASVType
from typing import Dict, List, Tuple, Iterator

import dbus.service

BUS_NAME = "org.freedesktop.portal.Desktop"
MAIN_OBJ = "/org/freedesktop/portal/desktop"
SYSTEM_BUS = False
MAIN_IFACE = "org.freedesktop.portal.Wallpaper"


def load(mock, parameters=None):
    # Delay before Request.response
    mock.delay: int = parameters.get("delay", 0)
    # Default response where none given
    mock.default_response: Tuple[int, ASVType] = parameters.get(
        "default-response", (0, {})
    )
    # Responses the methods, as a list, used up in-order
    responses: Dict[str, List[Tuple[int, ASVType]]] = parameters.get("responses", {})
    mock.responses: Dict[str, Iterator] = {m: iter(r) for m, r in responses.items()}

    mock.AddProperties(
        MAIN_IFACE,
        dbus.Dictionary({"version": dbus.UInt32(parameters.get("version", 1))}),
    )


@dbus.service.method(
    MAIN_IFACE,
    sender_keyword="sender",
    in_signature="ssa{sv}",
    out_signature="o",
)
def SetWallpaperURI(self, parent_window, uri, options, sender):
    request = Request(bus_name=self.bus_name, sender=sender, options=options)

    try:
        response = next(self.responses["SetWallpaperURI"])
    except (KeyError, StopIteration):
        response = self.default_response

    res, results = response
    request.respond(res, results, delay=self.delay)

    return request.handle


@dbus.service.method(
    MAIN_IFACE,
    sender_keyword="sender",
    in_signature="sha{sv}",
    out_signature="o",
)
def SetWallpaperFile(self, parent_window, fd, options, sender):
    request = Request(bus_name=self.bus_name, sender=sender, options=options)

    try:
        response = next(self.responses["SetWallpaperFile"])
    except (KeyError, StopIteration):
        response = self.default_response

    res, results = response
    request.respond(res, results, delay=self.delay)

    return request.handle
