# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbusmock import DBusMockObject
from typing import Dict, Any, Optional
from itertools import count
from gi.repository import GLib

import dbus
import logging


ASVType = Dict[str, Any]

logger = logging.getLogger("templates")
logger.setLevel(logging.DEBUG)
logging.basicConfig(format="%(levelname).1s|%(name)s: %(message)s", level=logging.DEBUG)


class Request:
    _token_counter = count()

    def __init__(
        self, bus_name: dbus.service.BusName, sender: str, options: Optional[ASVType]
    ):
        options = options or {}
        sender_token = sender.removeprefix(":").replace(".", "_")
        handle_token = options.get("handle_token", next(self._token_counter))
        self.handle = (
            f"/org/freedesktop/portal/desktop/request/{sender_token}/{handle_token}"
        )
        self.mock = DBusMockObject(
            bus_name=bus_name,
            path=self.handle,
            interface="org.freedesktop.portal.Request",
            props={},
        )
        self.mock.AddMethod("", "Close", "", "", "self.RemoveObject(self.path)")

    def respond(self, response: int, results: ASVType, delay: int = 0):
        def respond():
            logger.debug(
                f"Request.Response on {self.handle}: {response} results: {results}"
            )
            self.mock.EmitSignal(
                "", "Response", "ua{sv}", [dbus.UInt32(response), results]
            )

        if delay > 0:
            GLib.timeout_add(delay, respond)
        else:
            respond()


class Session:
    _token_counter = count()

    def __init__(
        self, bus_name: dbus.service.BusName, sender: str, options: Optional[ASVType]
    ):
        options = options or {}
        sender_token = sender.removeprefix(":").replace(".", "_")
        handle_token = options.get("session_handle_token", next(self._token_counter))
        self.handle = (
            f"/org/freedesktop/portal/desktop/session/{sender_token}/{handle_token}"
        )
        self.mock = DBusMockObject(
            bus_name=bus_name,
            path=self.handle,
            interface="org.freedesktop.portal.Session",
            props={},
        )
        self.mock.AddMethod("", "Close", "", "", "self.RemoveObject(self.path)")

    def close(self, details: ASVType, delay: int = 0):
        def respond():
            logger.debug(f"Session.Closed on {self.handle}: {details}")
            self.mock.EmitSignal("", "Closed", "a{sv}", [details])

        if delay > 0:
            GLib.timeout_add(delay, respond)
        else:
            respond()
