# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalpoker.cli import portal_poker
from click.testing import CliRunner
from . import return_values, TestPortal

import dbus


class TestNotification(TestPortal):
    def test_version_default(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner()
        result = runner.invoke(portal_poker, ["info", "Notification"])
        assert result.exit_code == 0
        assert "version = 1" in result.output

    def test_add_notification(self):
        params = {}
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            [
                "-v",
                "portal",
                "Notification",
                "AddNotification",
                "--wait-seconds=1",
                "my-id",
                "body text",
            ],
        )
        assert result.exit_code == 0

        method_calls = self.mock_interface.GetMethodCalls("AddNotification")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == "my-id"
        assert str(args[1]["body"]) == "body text"

    def test_add_notification_title(self):
        params = {}
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            [
                "-v",
                "portal",
                "Notification",
                "AddNotification",
                "--title=foo",
                "my-id",
                "body text",
            ],
        )
        assert result.exit_code == 0

        method_calls = self.mock_interface.GetMethodCalls("AddNotification")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == "my-id"
        assert str(args[1]["body"]) == "body text"
        assert str(args[1]["title"]) == "foo"

    def test_remove_notification(self):
        params = {}
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            [
                "-v",
                "portal",
                "Notification",
                "RemoveNotification",
                "my-id",
            ],
        )
        assert result.exit_code == 0

        method_calls = self.mock_interface.GetMethodCalls("RemoveNotification")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == "my-id"
