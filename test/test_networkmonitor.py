# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalpoker.cli import portal_poker
from click.testing import CliRunner
from . import return_values, TestPortal

import dbus


class TestNetworkMonitor(TestPortal):
    def test_version_default(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner()
        result = runner.invoke(portal_poker, ["info", "NetworkMonitor"])
        assert result.exit_code == 0
        assert "version = 3" in result.output

    def test_get_available(self):
        params = {}
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "NetworkMonitor", "GetAvailable"]
        )
        assert result.exit_code == 0
        assert result.stdout.strip().split("\n")[-1] == "yes"

    def test_get_available_false(self):
        params = {
            "return_values": return_values(
                {"GetAvailable": [{"available": dbus.Boolean(False)}]}
            ),
        }
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "NetworkMonitor", "GetAvailable"]
        )
        assert result.exit_code == 0
        assert result.stdout.strip().split("\n")[-1] == "no"

    def test_get_metered_false(self):
        params = {
            "return_values": return_values(
                {"GetMetered": [{"metered": dbus.Boolean(False)}]}
            ),
        }
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "NetworkMonitor", "GetMetered"]
        )
        assert result.exit_code == 0
        assert result.stdout.strip().split("\n")[-1] == "no"

    def test_get_connectivity_captive(self):
        params = {
            "return_values": return_values(
                {"GetConnectivity": [{"connectivity": dbus.UInt32(3)}]}
            ),
        }
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "NetworkMonitor", "GetConnectivity"]
        )
        assert result.exit_code == 0
        assert result.stdout.strip().split("\n")[-1] == "captive portal"

    def test_get_status(self):
        params = {
            "return_values": return_values(
                {
                    "GetStatus": [
                        {
                            "connectivity": dbus.UInt32(3),
                            "metered": dbus.Boolean(True),
                            "available": dbus.Boolean(True),
                        }
                    ]
                }
            ),
        }
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "NetworkMonitor", "GetStatus"]
        )
        assert result.exit_code == 0
        assert "available: yes" in result.stdout
        assert "metered: yes" in result.stdout
        assert "connectivity: local only" in result.stdout
