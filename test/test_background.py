# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalpoker.cli import portal_poker
from click.testing import CliRunner
from . import TestPortal


class TestBackground(TestPortal):
    def test_version_default(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner()
        result = runner.invoke(portal_poker, ["info", "Background"])
        assert result.exit_code == 0
        assert "version = 1" in result.output

    def test_request_background(self):
        params = {}
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "Background", "RequestBackground"]
        )
        assert result.exit_code == 0
        assert result.stdout.strip().split("\n")[-1] == "success"

    def test_request_background_reason(self):
        params = {}
        self.setup_daemon(params)

        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            [
                "-v",
                "portal",
                "Background",
                "RequestBackground",
                "--reason=foobar",
                "--autostart",
            ],
        )
        assert result.exit_code == 0
        assert result.stdout.strip().split("\n")[-1] == "success"

        method_calls = self.mock_interface.GetMethodCalls("RequestBackground")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == ""  # parent_window
        assert str(args[1]["reason"]) == "foobar"
        assert bool(args[1]["autostart"]) is True
