# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalpoker import load_introspection_xml
from typing import Any, Dict, List, Tuple
from gi.repository import GLib

import dbus
import dbusmock
import fcntl
import logging
import subprocess
import os
import unittest


def response(response: int, results: Dict[str, Any]) -> dbus.Dictionary:
    d = dbus.Dictionary(results, signature="sv")
    return dbus.Struct(
        [response, d],
        signature="ua{sv}",
    )


def responses(responses: Dict[str, List[Tuple[int, Any]]]):
    """
    Convert the given dictionary into a Dbus-compatible dictionary.
    """
    return dbus.Dictionary(
        {
            m: dbus.Array([response(r[0], r[1])], signature="(ua{sv})", variant_level=1)
            for m, rs in responses.items()
            for r in rs
        },
        signature="sv",
    )


def return_values(rvals: Dict[str, List[Tuple[int, Any]]]):
    """
    Convert the given dictionary into a Dbus-compatible dictionary.
    """
    return dbus.Dictionary(
        {
            m: dbus.Array(
                [dbus.Dictionary(r, signature="sv") for r in rs],
                signature="a{sv}",
                variant_level=1,
            )
            for m, rs in rvals.items()
            for r in rs
        },
        signature="sv",
    )


class TestPortal(dbusmock.DBusTestCase):
    @classmethod
    def setUpClass(cls):
        logging.basicConfig(
            format="%(levelname).1s|%(name)s: %(message)s", level=logging.DEBUG
        )

        cls.start_session_bus()
        if not os.environ.get("PORTALPOKER_TIMEOUT"):
            os.environ["PORTALPOKER_TIMEOUT"] = "2000"

    def setUp(self):
        self.p_mock = None

    def setup_daemon(self, params=None):
        portal = type(self).__name__.removeprefix("Test")

        try:
            load_introspection_xml(f"org.freedesktop.portal.{portal}.xml")
        except FileNotFoundError:
            raise unittest.SkipTest(f"Introspection XML for {portal} not found")

        self.p_mock, self.obj_portal = self.spawn_server_template(
            template=f"test/templates/{portal.lower()}.py",
            parameters=params,
            stdout=subprocess.PIPE,
        )
        flags = fcntl.fcntl(self.p_mock.stdout, fcntl.F_GETFL)
        fcntl.fcntl(self.p_mock.stdout, fcntl.F_SETFL, flags | os.O_NONBLOCK)
        self.mock_interface = dbus.Interface(self.obj_portal, dbusmock.MOCK_IFACE)
        self.portal_interface = dbus.Interface(
            self.obj_portal, "org.freedesktop.portal.{portal}"
        )

    def tearDown(self):
        if self.p_mock:
            if self.p_mock.stdout:
                out = (self.p_mock.stdout.read() or b"").decode("utf-8")
                if out:
                    print(out)
                self.p_mock.stdout.close()
            self.p_mock.terminate()
            self.p_mock.wait()
