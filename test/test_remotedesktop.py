# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalpoker.cli import portal_poker
from click.testing import CliRunner
from . import responses, TestPortal


class TestRemoteDesktop(TestPortal):
    def test_version_default(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner()
        result = runner.invoke(portal_poker, ["info", "RemoteDesktop"])
        assert result.exit_code == 0
        assert "version = 1" in result.output

    def test_available_types(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "RemoteDesktop", "AvailableDeviceTypes"]
        )
        assert result.exit_code == 0
        stdout = result.stdout.strip()
        assert "keyboard" in stdout
        assert "pointer" in stdout
        assert "touchscreen" in stdout

    def test_available_types_pointer_only(self):
        params = {"AvailableDeviceTypes": 0x2}
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "RemoteDesktop", "AvailableDeviceTypes"]
        )
        assert result.exit_code == 0
        stdout = result.stdout.strip()
        assert "keyboard" not in stdout
        assert "pointer" in stdout
        assert "touchscreen" not in stdout

    def test_select_devices(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            ["-v", "portal", "RemoteDesktop", "SelectDevices", "mt"],
        )
        assert result.exit_code == 0

        method_calls = self.mock_interface.GetMethodCalls("CreateSession")
        assert len(method_calls) == 1
        method_calls = self.mock_interface.GetMethodCalls("SelectDevices")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert int(args[1]["types"]) == 0x6

    def test_start(self):
        params = {
            "responses": responses(
                {
                    "Start": [(0, {"devices": 0x6})],
                }
            )
        }
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            ["-v", "portal", "RemoteDesktop", "Start"],
        )
        assert result.exit_code == 0

        method_calls = self.mock_interface.GetMethodCalls("CreateSession")
        assert len(method_calls) == 1
        method_calls = self.mock_interface.GetMethodCalls("SelectDevices")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert int(args[1]["types"]) == 0x7  # mkt

        # Test we call with the same session handle each time
        session_handle = str(args[0])

        method_calls = self.mock_interface.GetMethodCalls("Start")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == session_handle

    def test_notify_pointer_motion(self):
        params = {
            "responses": responses(
                {
                    "Start": [(0, {"devices": 0x6})],
                }
            )
        }
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            ["-v", "portal", "RemoteDesktop", "NotifyPointerMotion", "1,0", "5,10"],
        )
        assert result.exit_code == 0

        method_calls = self.mock_interface.GetMethodCalls("CreateSession")
        assert len(method_calls) == 1
        method_calls = self.mock_interface.GetMethodCalls("SelectDevices")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert int(args[1]["types"]) == 0x7  # mkt

        # Test we call with the same session handle each time
        session_handle = str(args[0])

        method_calls = self.mock_interface.GetMethodCalls("Start")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == session_handle

        method_calls = self.mock_interface.GetMethodCalls("NotifyPointerMotion")
        assert len(method_calls) == 2
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == session_handle
        assert float(args[2]) == 1.0
        assert float(args[3]) == 0.0
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == session_handle
        assert float(args[2]) == 5.0
        assert float(args[3]) == 10.0
