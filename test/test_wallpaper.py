# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalpoker.cli import portal_poker
from click.testing import CliRunner
from . import responses, TestPortal
import os


class TestWallpaper(TestPortal):
    def test_version_default(self):
        params = {}
        self.setup_daemon(params)
        runner = CliRunner()
        result = runner.invoke(portal_poker, ["info", "Wallpaper"])
        assert result.exit_code == 0
        assert "version = 1" in result.output

    def test_version_custom(self):
        params = {"version": 22}
        self.setup_daemon(params)
        runner = CliRunner()
        result = runner.invoke(portal_poker, ["info", "Wallpaper"])
        assert result.exit_code == 0
        assert "version = 22" in result.output

    def test_set_file(self):
        params = {}

        filename = __file__  # guaranteed to exist

        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "Wallpaper", "SetWallpaperFile", filename]
        )
        assert result.exit_code == 0
        assert result.stdout.strip() == "success"

        method_calls = self.mock_interface.GetMethodCalls("SetWallpaperFile")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == ""  # parent_window
        fd = args[1]
        assert os.readlink(f"/proc/self/fd/{fd.take()}") == filename

    def test_set_uri(self):
        uri = "https://does.not.exist"
        params = {}

        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "Wallpaper", "SetWallpaperURI", uri]
        )
        assert result.exit_code == 0
        assert result.stdout.strip() == "success"

        method_calls = self.mock_interface.GetMethodCalls("SetWallpaperURI")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == ""  # parent_window
        assert str(args[1]) == uri

    # dbusmock is UnitTest so we can't use pytest.mark.parametrize and
    # unitest.subTest() is no good
    def test_set_uri_response_1(self):
        uri = "https://does.not.exist"
        params = {
            "responses": responses(
                {
                    "SetWallpaperURI": [(1, {})],
                }
            ),
        }
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "Wallpaper", "SetWallpaperURI", uri]
        )
        assert result.exit_code == 0  # effing click always exists with 0
        stdout = result.stdout.strip()
        assert stdout == "cancelled"

    # dbusmock is UnitTest so we can't use pytest.mark.parametrize and
    # unitest.subTest() is no good
    def test_set_uri_response_2(self):
        uri = "https://does.not.exist"
        params = {
            "responses": responses(
                {
                    "SetWallpaperURI": [(2, {})],
                }
            ),
        }

        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "Wallpaper", "SetWallpaperURI", uri]
        )
        assert result.exit_code == 0  # effing click always exists with 0
        stdout = result.stdout.strip()
        assert stdout == "error"

    def test_set_uri_cancel(self):
        uri = "https://does.not.exist"
        params = {
            "delay": 10000,
        }

        env = {
            "PORTALPOKER_REQUEST_TIMEOUT": "200",
        }

        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker, ["-v", "portal", "Wallpaper", "SetWallpaperURI", uri], env=env
        )
        assert result.exit_code == 0
        assert result.stdout.strip() == "cancelled"

    def test_set_uri_preview(self):
        uri = "https://does.not.exist"
        params = {}
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            ["-v", "portal", "Wallpaper", "SetWallpaperURI", "--preview", uri],
        )
        assert result.exit_code == 0  # effing click always exists with 0

        method_calls = self.mock_interface.GetMethodCalls("SetWallpaperURI")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == ""  # parent_window
        assert str(args[1]) == uri
        assert bool(args[2]["show-preview"]) is True

    def test_set_uri_no_preview(self):
        uri = "https://does.not.exist"
        params = {}
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            ["-v", "portal", "Wallpaper", "SetWallpaperURI", "--no-preview", uri],
        )
        assert result.exit_code == 0  # effing click always exists with 0

        method_calls = self.mock_interface.GetMethodCalls("SetWallpaperURI")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == ""  # parent_window
        assert str(args[1]) == uri
        assert bool(args[2]["show-preview"]) is False

    def test_set_uri_set_on_background(self):
        uri = "https://does.not.exist"
        params = {}
        self.setup_daemon(params)
        runner = CliRunner(mix_stderr=False)
        result = runner.invoke(
            portal_poker,
            [
                "-v",
                "portal",
                "Wallpaper",
                "SetWallpaperURI",
                "--set-on=background",
                uri,
            ],
        )
        assert result.exit_code == 0  # effing click always exists with 0

        method_calls = self.mock_interface.GetMethodCalls("SetWallpaperURI")
        assert len(method_calls) == 1
        timestamp, args = method_calls.pop(0)
        assert str(args[0]) == ""  # parent_window
        assert str(args[1]) == uri
        assert args[2]["set-on"] == "background"
