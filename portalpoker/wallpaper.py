# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

import dbus
from pathlib import Path

from portalpoker import Request, PortalPoker

import click
import logging

logger = logging.getLogger("Wallpaper")


@click.group(name="Wallpaper")
@click.pass_context
def wallpaper(ctx):
    pp = ctx.obj
    pp.portal = pp.load_portal("org.freedesktop.portal.Wallpaper")


@wallpaper.command(name="SetWallpaperURI")
@click.argument("uri", type=str)
@click.option("--preview/--no-preview", default=True)
@click.option(
    "--set-on",
    type=click.Choice(["background", "lockscreen", "both"]),
    default="background",
)
@click.pass_context
def set_wallpaper_uri(ctx, uri: str, preview: bool, set_on: str):
    pp = ctx.obj
    portal = pp.portal

    def result(result):
        print(result.response_string)
        pp.quit()

    portal.request(
        "SetWallpaperURI",
        callback=result,
        parent_window="",
        uri=uri,
        options={
            "set-on": dbus.String(set_on, variant_level=1),
            "show-preview": dbus.Boolean(preview, variant_level=1),
        },
    )
    pp.run()


@wallpaper.command(name="SetWallpaperFile")
@click.argument("filename", type=click.Path(exists=True))
@click.option("--preview/--no-preview", default=True)
@click.option(
    "--set-on",
    type=click.Choice(["background", "lockscreen", "both"]),
    default="background",
)
@click.pass_context
def set_wallpaper_file(ctx, filename: Path, preview: bool, set_on: str):
    pp = ctx.obj
    portal = pp.portal

    with open(filename) as fd:

        def result(result):
            print(result.response_string)
            pp.quit()

        portal.request(
            "SetWallpaperFile",
            callback=result,
            parent_window="",
            fd=fd.fileno(),
            options={
                "set-on": dbus.String(set_on, variant_level=1),
                "show-preview": dbus.Boolean(preview, variant_level=1),
            },
        )
        pp.run()


# ############## impl.portal ############### #


@click.group(name="Wallpaper")
@click.pass_context
def impl_wallpaper(ctx):
    interface = "org.freedesktop.impl.portal.Wallpaper"
    if not ctx.obj:
        pp = PortalPoker.create_for_impl(interface)
        ctx.obj = pp
    pp.portal = pp.load_portal(interface)


@impl_wallpaper.command(name="SetWallpaperURI")
@click.argument("appid", type=str)
@click.argument("uri", type=str)
@click.option("--preview/--no-preview", default=True)
@click.option(
    "--set-on",
    type=click.Choice(["background", "lockscreen", "both"]),
    default="background",
)
@click.pass_context
def impl_set_wallpaper_uri(ctx, appid: str, uri: str, preview: bool, set_on: str):
    pp = ctx.obj
    portal = pp.portal

    with Request.new_impl(portal.bus, portal.busname) as request:
        portal.method(
            "SetWallpaperURI",
            callback=None,
            handle=request.handle,
            app_id=appid,
            parent_window="x",
            uri=uri,
            options={
                "set-on": dbus.String(set_on, variant_level=1),
                "show-preview": dbus.String(preview, variant_level=1),
            },
        )
