#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus.mainloop.glib import DBusGMainLoop
from pathlib import Path
from gi.repository import GLib
from typing import Any, Callable, Dict, List, Optional
from itertools import count

import dbus
import attr
import logging
import os
import re
import xml.etree.ElementTree as ET


ResponseCallback = Callable[["Response"], None]
MethodCallback = Callable[[List[Any]], None]

logger = logging.getLogger("portal-poker.core")

basedir = Path("/usr/share/dbus-1/interfaces")

_CACHED_INTROSPECTION_FILES: Dict[str, str] = {}

DBusGMainLoop(set_as_default=True)


def load_introspection_xml(filename: str):
    try:
        introspection = _CACHED_INTROSPECTION_FILES[filename]
    except KeyError:
        data_dirs = os.getenv("XDG_DATA_DIRS", "/usr/share").split(":")
        for data_dir in data_dirs:
            try:
                with open(f"{data_dir}/dbus-1/interfaces/{filename}") as f:
                    introspection = f.read()
                _CACHED_INTROSPECTION_FILES[filename] = introspection
                break
            except FileNotFoundError:
                pass
        else:
            raise FileNotFoundError(f"Unable to find portal definition {filename}")
    return introspection


class PortalError(Exception):
    pass


@attr.s
class HandleToken:
    _counter = count()
    token: str = attr.ib()

    @classmethod
    def create(cls):
        return cls(token=f"token{next(cls._counter)}")


@attr.s
class SenderToken:
    token: str = attr.ib()

    @classmethod
    def from_bus(cls, bus: dbus.Bus):
        assert bus.get_unique_name() is not None
        return cls.from_sender_string(bus.get_unique_name())

    @classmethod
    def from_sender_string(cls, sender: str):
        sender_token = sender.removeprefix(":").replace(".", "_")
        return cls(token=sender_token)


@attr.s
class Portal:
    _CACHED_FILES: Dict[str, str] = {}
    bus = attr.ib()
    intf = attr.ib()
    proxy = attr.ib()

    @property
    def busname(self) -> str:
        return self.bus.get_unique_name()

    @property
    def name(self) -> str:
        return self.intf.dbus_interface

    @property
    def version(self) -> int:
        properties_intf = dbus.Interface(self.proxy, "org.freedesktop.DBus.Properties")
        return properties_intf.Get(self.name, "version")

    @classmethod
    def _dbus_name(cls, name: str) -> str:
        """CamelCase to snake_case"""
        return re.sub(r"([A-Z]+)", r"_\1", name).lower()

    def property(self, name: str) -> Any:
        """
        Return the value of the property with the given name
        """
        properties_intf = dbus.Interface(self.proxy, "org.freedesktop.DBus.Properties")
        return properties_intf.Get(self.name, name)

    def method(
        self, method_name: str, callback: Optional[MethodCallback] = None, **kwargs
    ) -> Any:
        """
        Call the given method name with the kwargs (in-order) as arguments and
        return its values, if any.

        If callback is None, the method is run as sync version, otherwise the
        callback is invoked with the results of the call.
        """
        args = [v for k, v in kwargs.items()]
        logger.debug(f"Calling {method_name} with args: {args}")
        if callback:

            def method_async_callback(result):
                logger.debug(f"-> Result for {method_name}: {result}")
                assert callback
                callback(result)

            def error_callback(error):
                logger.critical(error)
                raise error

            method = getattr(self.intf, method_name)
            method(
                *args, reply_handler=method_async_callback, error_handler=error_callback
            )
            return None
        else:
            method = getattr(self.intf, method_name)
            results = method(*args)
            logger.debug(f"-> Result: {results}")
            return results

    def method_noreply(self, method_name: str, **kwargs) -> None:
        """
        Call the given no-reply method name with the kwargs (in-order) as
        arguments and return its values, if any.
        """
        args = [v for k, v in kwargs.items()]
        logger.debug(f"Calling {method_name} with args: {args}")
        method = getattr(self.intf, method_name)
        method(*args, ignore_reply=True)

    def signal(self, name: str, callback: Callable) -> None:
        """
        Hook up the callback to the signal with the given name.
        """

        signal_match = None

        def signal_cb(*args):
            callback(*args)
            # FIXME: is a once-off good enough?
            self.bus.remove_signal_receiver(signal_match)

        signal_match = self.bus.add_signal_receiver(
            signal_cb, dbus_interface=self.name, signal_name=name
        )

    def request(
        self, name: str, callback: Optional[ResponseCallback], **kwargs
    ) -> "Request":
        """
        Call the given method name with the kwargs (in-order) as arguments.

        If a callback is given it is invoked with the Result of the Response.
        """
        method = getattr(self.intf, f"{name}")

        def result_callback(result):
            if result.response != result.SUCCESS:
                logger.error(f"Request failed with: {result.response}")

            if callback is not None:
                callback(result)

        r = Request.new_with_callback(self.bus, self.busname, result_callback)
        try:
            options = kwargs["options"]
        except KeyError:
            options = {}
            kwargs["options"] = options
        options["handle_token"] = dbus.String(r.token, variant_level=1)

        args = [v for k, v in kwargs.items() if k != "callback"]
        logger.debug(f"Requesting {name} with args: {args}")

        def request_async_callback(handle: str):
            if handle != r.handle:
                logger.critical("Unexpected request handle {handle}")

        def error_callback(error):
            raise error

        method(
            *args, reply_handler=request_async_callback, error_handler=error_callback
        )

        logger.debug(f"Request is at {r.handle}")

        return r

    def __str__(self) -> str:
        str = [f"{self.name}:", "  Properties:", f"    version = {self.version}"]

        introspection = self.proxy.Introspect(
            dbus_interface="org.freedesktop.DBus.Introspectable"
        )
        root = ET.fromstring(introspection)

        for interface in root.findall("interface"):
            if interface.get("name") != self.name:
                continue

            for p in interface.findall("property"):
                name = p.get("name")
                signature = p.get("type")
                if name != "version":
                    str += [f"    {name} = {signature}"]

            if interface.find("method"):
                str += ["  Methods: "]

            for m in interface.findall("method"):
                name = m.get("name")
                in_sig = ", ".join(
                    [
                        f"{a.get('name')}: {a.get('type')}"
                        for a in m.findall("arg")
                        if a.get("direction") == "in"
                    ]
                )
                out_sig = ", ".join(
                    [
                        f"{a.get('name')}: {a.get('type')}"
                        for a in m.findall("arg")
                        if a.get("direction") == "out"
                    ]
                )
                if not out_sig:
                    out_sig = "None"
                m_sig = f"    {name}({in_sig}) -> {out_sig}"
                str += [m_sig]

            if interface.find("signal"):
                str += ["  Signals: "]

            for s in interface.findall("signal"):
                name = s.get("name")
                sig = ", ".join(
                    [f"{a.get('name')}: {a.get('type')}" for a in m.findall("arg")]
                )
                str += [f"    {name}: {sig}"]

        return "\n".join(str)

    @classmethod
    def from_file(
        cls, bus: dbus.Bus, busname: str, objpath: str, file: str
    ) -> "Portal":
        try:
            introspection = load_introspection_xml(file)
        except FileNotFoundError as e:
            raise PortalError(str(e))

        root = ET.fromstring(introspection)
        iface = root.find("interface")
        if not iface:
            raise PortalError(f"Invalid XML file {file}")
        iname = iface.get("name")
        if not iname:
            raise PortalError(f"Invalid XML file {file}")

        logger.debug(f"Loading interface: {iname}")

        proxy = bus.get_object(busname, objpath)
        interface = dbus.Interface(proxy, iname)

        return cls(bus=bus, intf=interface, proxy=proxy)

    @classmethod
    def from_portal_name(cls, bus: dbus.Bus, busname: str, name: str) -> "Portal":
        objpath = "/org/freedesktop/portal/desktop"

        return cls.from_file(bus, busname, objpath, f"{name}.xml")


@attr.s
class Response:
    """
    The response returned for a Request, if any
    """

    SUCCESS = 0
    CANCELLED = 1
    ENDED = 2

    response: int = attr.ib()
    results: Dict[str, Any] = attr.ib()

    @property
    def response_string(self) -> str:
        if self.response == 0:
            return "success"
        elif self.response == 1:
            return "cancelled"
        elif self.response == 2:
            return "error"
        raise PortalError(f"Invalid response code {self.response}")


@attr.s
class Request:
    _counter = count()
    id: int = attr.ib(
        init=False,
        default=attr.Factory(lambda self: next(self._counter), takes_self=True),
    )
    token: str = attr.ib()
    handle: str = attr.ib()
    _portal: Portal = attr.ib()
    _is_closed: bool = attr.ib(init=False, default=False)

    def close(self) -> None:
        if self._is_closed:
            return

        self._is_closed = True

        def noop(*args, **kwargs):
            pass

        logger.debug(f"Closing request {self.id}")
        self._portal.intf.Close(reply_handler=noop, error_handler=noop)

    def __enter__(self) -> "Request":
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.close()

    @classmethod
    def new_with_callback(
        cls, bus: dbus.Bus, busname: str, callback: Callable
    ) -> "Request":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        request_objpath = (
            f"/org/freedesktop/portal/desktop/request/{sender_token}/{handle_token}"
        )

        request = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=request_objpath,
            file="org.freedesktop.portal.Request.xml",
        )

        r = Request(token=handle_token, handle=request_objpath, portal=request)

        # Used for testing so we can test Request.Close
        timeout = int(os.getenv("PORTALPOKER_REQUEST_TIMEOUT", 0))
        if timeout > 0:

            def cb_timeout():
                logger.debug(f"Closing request {r.handle} after timeout")
                callback(Response(1, {}))
                r.close()

            GLib.timeout_add(timeout, cb_timeout)

        sigmatch = None

        def set_results(response, results, path):
            if path != r.handle:
                return

            logger.debug(f"Response for request {r.id}: {response} {results}")
            callback(Response(response, results))

            bus.remove_signal_receiver(sigmatch)

            r.close()

        sigmatch = bus.add_signal_receiver(
            set_results,
            path_keyword="path",
            dbus_interface="org.freedesktop.portal.Request",
            signal_name="Response",
        )
        return r

    @classmethod
    def new_impl(cls, bus: dbus.Bus, busname: str) -> "Request":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        request_objpath = (
            f"/org/freedesktop/portal/desktop/request/{sender_token}/{handle_token}"
        )

        request = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=request_objpath,
            file="org.freedesktop.impl.portal.Request.xml",
        )

        return Request(token=handle_token, handle=request_objpath, portal=request)


@attr.s
class Session:
    _counter = count()
    id: int = attr.ib(
        init=False,
        default=attr.Factory(lambda self: next(self._counter), takes_self=True),
    )
    token: str = attr.ib()
    handle: str = attr.ib()
    _portal = attr.ib()
    _is_closed: bool = attr.ib(init=False, default=False)

    @property
    def session_handle(self) -> str:
        return self.handle

    def close(self) -> None:
        if self._is_closed:
            return

        self._is_closed = True

        def noop(*args, **kwargs):
            pass

        logger.debug(f"Closing session {self.id}")
        self._portal.intf.Close(reply_handler=noop, error_handler=noop)

    def __enter__(self) -> "Session":
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.close()

    def add_token_to_options(self, options: Optional[Dict[str, Any]]) -> Dict[str, Any]:
        if not options:
            options = {}
        options["session_handle_token"] = dbus.String(self.token, variant_level=1)
        return options

    @classmethod
    def new(cls, bus: dbus.Bus, busname: str) -> "Session":
        return cls.new_with_callback(bus, busname)

    @classmethod
    def new_with_callback(
        cls, bus: dbus.Bus, busname: str, closed_callback: Optional[Callable] = None
    ) -> "Session":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        session_objpath = (
            f"/org/freedesktop/portal/desktop/session/{sender_token}/{handle_token}"
        )

        session = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=session_objpath,
            file="org.freedesktop.portal.Session.xml",
        )

        s = Session(token=handle_token, handle=session_objpath, portal=session)

        sigmatch = None

        def on_closed(path):
            if path != s.handle:
                return

            logger.debug(f"Session {s.id}: Closed")
            if closed_callback:
                closed_callback()

            bus.remove_signal_receiver(sigmatch)

            s.close()

        sigmatch = bus.add_signal_receiver(
            on_closed,
            path_keyword="path",
            dbus_interface="org.freedesktop.portal.Session",
            signal_name="Closed",
        )
        return s

    @classmethod
    def new_impl(cls, bus: dbus.Bus, busname: str) -> "Session":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        session_objpath = (
            f"/org/freedesktop/portal/desktop/session/{sender_token}/{handle_token}"
        )

        session = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=session_objpath,
            file="org.freedesktop.impl.portal.Session.xml",
        )

        return Session(token=handle_token, handle=session_objpath, portal=session)


@attr.s
class PortalPoker:
    bus: dbus.Bus = attr.ib()
    busname: str = attr.ib()

    def run(self, timeout: int = 0) -> None:
        try:
            timeout = int(os.getenv("PORTALPOKER_TIMEOUT", timeout))
            if timeout > 0:
                self.add_timeout(timeout, self.quit)
            self.mainloop = GLib.MainLoop()
            self.mainloop.run()
        except KeyboardInterrupt:
            pass

    def add_timeout(self, millis: int, callback: Callable) -> None:
        GLib.timeout_add(millis, callback)

    def quit(self) -> None:
        GLib.idle_add(self.mainloop.quit)

    def show_portal_info(self, name: str) -> str:
        return str(Portal.from_portal_name(self.bus, self.busname, name))

    def load_portal(self, name: str) -> Portal:
        return Portal.from_portal_name(bus=self.bus, busname=self.busname, name=name)

    @classmethod
    def create(cls, busname: str) -> "PortalPoker":
        try:
            logger.debug(f"{os.environ.get('DBUS_SESSION_BUS_ADDRESS')}")
            try:
                # From a dbusmock comment:
                # "This is preferrable to dbus.SystemBus() and dbus.SessionBus() as those
                # do not get along with multiple changing local test buses."
                # using get_session() results in our test suite failure, after
                # the first tearDownClass() the bus changes and we get
                # disconnected
                addr = os.environ["DBUS_SESSION_BUS_ADDRESS"]
                bus = dbus.bus.BusConnection(addr)
            except KeyError:
                bus = dbus.Bus.get_session()
            logger.debug(f"Connected to DBus {busname}, we are {bus.get_unique_name()}")
        except dbus.DBusException as e:
            logger.critical(e)
            raise PortalError(f"Failed to connect to DBus: {e}")
        except Exception as e:
            logger.critical(e)
            raise e

        return cls(bus=bus, busname=busname)

    @classmethod
    def create_for_impl(cls, interface: str) -> "PortalPoker":
        from configparser import ConfigParser

        desktop_type = os.getenv("XDG_SESSION_DESKTOP", "gnome")
        data_dirs = os.getenv("XDG_DATA_DIRS", "/usr/share").split(":")
        for data_dir in data_dirs:
            portals_dir = f"{data_dir}/xdg-desktop-portal/portals"
            for portal_file in Path(portals_dir).glob("*.portal"):
                config = ConfigParser()
                config.read(portal_file)
                try:
                    if config["portal"]["UseIn"] != desktop_type:
                        continue
                    interfaces = config["portal"]["Interfaces"].split(";")
                    if interface in interfaces:
                        logger.debug(f"Found requested interface in {portal_file}")
                        busname = config["portal"]["DBusName"]
                        return cls.create(busname)
                except KeyError:
                    pass

        raise PortalError(f"Unable to find bus name for {interface}")
